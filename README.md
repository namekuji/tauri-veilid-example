# Tauri + Preact + Typescript

This template should help get you started developing with Tauri, Preact and Typescript in Vite.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)

## How to Use

1. Set up a [veilid-server](https://gitlab.com/veilid/veilid/-/blob/main/INSTALL.md).
2. Run `pnpm i && pnpm tauri dev` and then press `Connect` on the main window.
3. Note the Node ID shown in the main window.
4. Send a message using `veilid-cli` via `appmessage <your_node_id> <message>`.
5. Your message will appear in the main window if everything is working.
