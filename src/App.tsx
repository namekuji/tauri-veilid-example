import { useEffect, useState } from "preact/hooks";
import preactLogo from "./assets/preact.svg";
import { invoke } from "@tauri-apps/api/core";
import { getCurrent } from '@tauri-apps/api/window';
import "./App.css";

function App() {
  const [message, setMessage] = useState("");
  const [nodeId, setNodeId] = useState("");
  const [connected, setConnected] = useState(false);

  useEffect(() => {
    invoke<string>("get_node_id").then((id) => setNodeId(id));
  }, []);

  const appWindow = getCurrent();

  async function connect() {
    if (!connected) {
      await invoke("connect_veilid");
      await appWindow.listen<string>("app-message", (event) => {
        setMessage(event.payload);
      });
      setConnected(true);
    }
  }

  return (
    <div class="container">
      <h1>Welcome to Tauri!</h1>

      <div class="row">
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" class="logo vite" alt="Vite logo" />
        </a>
        <a href="https://tauri.app" target="_blank">
          <img src="/tauri.svg" class="logo tauri" alt="Tauri logo" />
        </a>
        <a href="https://preactjs.com" target="_blank">
          <img src={preactLogo} class="logo preact" alt="Preact logo" />
        </a>
      </div>

      <button type="button" onClick={connect} disabled={connected}>
        {connected ? "Connected" : "Connect"}
      </button>

      <p>Node ID: {nodeId}</p>
      <p>{message}</p>
    </div>
  );
}

export default App;
